/**
 * Configuración para DataTable con función
 * de cargar, editar y eliminar registros.
 */

let getUrl = window.location;
let baseUrl = getUrl.protocol + "//" + getUrl.host + "/";

let positionsArrObjs, positionsJsonstring;

$.post(baseUrl + "admin/positions/all-dt", (data) => {
  // El DATA es recibido como un STRING
  // y es transformado en OBJECT para actuar sobre su "records"
  positionsArrObjs = JSON.parse(data).records;
  positionsJsonstring = fromArrObjToJsonString(positionsArrObjs);

  function fromArrObjToJsonString(positionsArrObjs) {
    let _positions = {};
    for (let item of positionsArrObjs) {
      _positions[item["id"]] = item["description"];
    }
    return JSON.stringify(_positions);
  }

  function toFormatDate(_date) {
    //console.log(_date)//2007-06-04 13:38:12.00000
    let date = new Date(_date);
    //console.log(date)//Mon Jun 04 2007 13:38:12 GMT+0200 (hora de verano de Europa central)
    let day = date.getDate() < 10 ? `0${date.getDate()}` : date.getDate();
    let month =
      date.getMonth() + 1 < 10
        ? `0${date.getMonth() + 1}`
        : date.getMonth() + 1;
    let year = date.getFullYear();
    //console.log(`${day}/${month}/${year}`)
    return `${day}/${month}/${year}`;
  }

  $("#tableListAndEdit tbody").html("");

  $("#tableListAndEdit").DataTable({
    processing: true,
    serverSide: true,
    language: {
      url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
    },
    lengthMenu: [
      [5, 10, 20, -1],
      [5, 10, 20, "Todo"],
    ],
    ajax: {
      url: baseUrl + "admin/employees/all-dt-ssp",
      type: "POST",
    },
    initComplete: () => {
      console.log(":-)");
      // alert("Aloha ... DataTables cargado");
      document.getElementById("tableListAndEdit").removeAttribute("style");
    },
  });

  $("#tableListAndEdit").on("draw.dt", () => {
    $("#tableListAndEdit").Tabledit({
      url: baseUrl + "admin/employees/all-dt-ssp-actions",
      dataType: "json",
      //editmethod: "get",
      columns: {
        identifier: [0, "id"],
        //editable: [[2, 'age'], [5, 'salary'],]
        //-----------------------------------------------------------
        // Con elemento SELECT para la columna de "Puesto",
        // teniendo el array de datos de la tabla correspondiente
        ////editable: [[2, 'age'], [4, 'position_id', 'select', {'1': 'Consultor@ de Dirección', '2': 'Administración', '3': 'Diseñador@ UI', '4': 'Ingenier@ de Software', '5': 'Comercial'}], [5, 'salary'],],
        editable: [
          // [3, "registrationDate", "date"],
          [3, "registrationDate"],
          [
            4,
            "position_id",
            "select",
            ////'{"1": "Consultor@ de Dirección", "2": "Administrador@", "3": "Diseñador@ UI", "4": "Ingenier@ de Software", "5": "Comercial"}',
            //'{"1": "Consultor@ de Dirección", "2": "Administrador@", "3": "Diseñador@ UI", "4": "Ingenier@ de Software", "5": "Comercial"}',
            positionsJsonstring,
            //"{}",
          ],
          [5, "salary"],
        ],
      },
      restoreButton: false,
      // ====================================================================
      // toolbarClass: "",
      lang: "es",
      buttons: {
        edit: {
          class: "btn btn-sm btn-default",
          html: '<i class="fas fa-edit" title="Editar registro"></i>',
          action: "edit",
        },
        delete: {
          class: "btn btn-sm btn-default",
          html: '<i class="fas fa-trash" title="Borrar registro"></i>',
          action: "delete",
        },
        save: {
          class: "btn btn-sm btn-success",
          //html: "Save",
        },
        restore: {
          class: "btn btn-sm btn-warning",
          html: "Restore",
          action: "restore",
        },
        confirm: {
          class: "btn btn-sm btn-danger",
          //html: "Confirm",
        },
      },
      // ====================================================================
      onSuccess: (data, textStatus, jqXHR) => {
        // console.log('onSuccess(data, textStatus, jqXHR)');
        // console.log(data);
        // console.log(textStatus);
        // console.log(jqXHR);
        if (data.action == "delete") {
          $("#" + data.id).remove();
          $("#tableListAndEdit").DataTable().ajax.reload();
        }
      },
      // onFail: function (jqXHR, textStatus, errorThrown) {
      //   console.log("onFail(jqXHR, textStatus, errorThrown)");
      //   console.log(jqXHR);
      //   console.log(textStatus);
      //   //console.log(errorThrown);
      // },
      // onAlways: function() {
      //     console.log('onAlways()');
      // },
      // onAjax: function (action, serialize) {
      //   console.log("onAjax(action, serialize)");
      //   console.log(action);
      //   console.log(serialize);
      // },
    });
  });
});
