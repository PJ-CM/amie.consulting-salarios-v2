import Chart from "chart.js";

let salariesAveragesChart = document.getElementById("salariesMaxMinAvgChart");
let totAgeRangeChart = document.getElementById("totEmployeesByAgeRangeChart");
let avgSalaryAgeRangeChart = document.getElementById(
  "averageSalaryByAgeRangeChart"
);

$.post("charts/max-min-avg-salaries", (data) => {
  let _data = [];
  let obj = JSON.parse(data);
  // _data.push(obj[0]);
  $.each(obj, (test, item) => {
    _data.push(item);
  });

  let myChart = new Chart(salariesAveragesChart, {
    type: "bar",
    data: {
      //   labels: ["Suma de Sueldos", "Sueldo MÁX.", "Sueldo MÍN.", "Sueldo MEDIO"],
      labels: ["Sueldo MÁX.", "Sueldo MÍN.", "Sueldo MEDIO"],
      datasets: [
        {
          //label: "Sueldos Totales - Máx - Mín - Medio",
          //   label: "Sueldos: Máx - Mín - Medio",
          label: "",
          //data: _data,
          data: [
            // _data[0]["suma"],
            //--------------------------------------------
            // _data[0]["max"],
            // _data[0]["min"],
            //_data[0]["avg"],
            parseFloat(_data[0]["max"]).toFixed(2),
            parseFloat(_data[0]["min"]).toFixed(2),
            parseFloat(_data[0]["avg"]).toFixed(2),
          ],
          backgroundColor: [
            //"rgba(255, 99, 132, 0.2)",
            "rgba(255, 206, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(153, 102, 255, 0.2)",
          ],
          borderColor: [
            //"rgba(255, 99, 132, 1)",
            "rgba(255, 206, 86, 1)",
            "rgba(75, 192, 192, 1)",
            "rgba(153, 102, 255, 1)",
          ],
          borderWidth: 1,
        },
      ],
    },
    options: {
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
      legend: {
        display: false,
      },
    },
  });
});

$.post("charts/tot-employees-by-age-range", (data) => {
  let _data = [];
  let obj = JSON.parse(data);
  // _data.push(obj[0]);
  $.each(obj, (test, item) => {
    _data.push(item.TOT);
  });

  let myChart = new Chart(totAgeRangeChart, {
    type: "doughnut",
    data: {
      //   labels: ["Suma de Sueldos", "Sueldo MÁX.", "Sueldo MÍN.", "Sueldo MEDIO"],
      labels: ["Entre 18 y 30", "Entre 31 y 50", "Más de 50"],
      datasets: [
        {
          //label: "Total de Empleados por Rango de Edad",
          data: _data,
          backgroundColor: [
            //"rgba(255, 99, 132, 0.2)",
            "rgba(255, 206, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(153, 102, 255, 0.2)",
          ],
          borderColor: [
            //"rgba(255, 99, 132, 1)",
            "rgba(255, 206, 86, 1)",
            "rgba(75, 192, 192, 1)",
            "rgba(153, 102, 255, 1)",
          ],
          borderWidth: 1,
        },
      ],
    },
    // options: {
    //   scales: {
    //     yAxes: [
    //       {
    //         ticks: {
    //           beginAtZero: true,
    //         },
    //       },
    //     ],
    //   },
    // },
  });
});

$.post("charts/average-salary-by-age-range", (data) => {
  let _dataLabels = [];
  let _dataValues = [];
  let obj = JSON.parse(data);
  // _data.push(obj[0]);
  $.each(obj, (test, item) => {
    _dataLabels.push(item.age_rango);
    _dataValues.push(parseFloat(item.avg_salary).toFixed(2));
  });

  let myChart = new Chart(avgSalaryAgeRangeChart, {
    type: "line",
    data: {
      //   labels: ["Suma de Sueldos", "Sueldo MÁX.", "Sueldo MÍN.", "Sueldo MEDIO"],
      labels: _dataLabels,
      datasets: [
        {
          //label: "Total de Empleados por Rango de Edad",
          data: _dataValues,
          backgroundColor: [
            "rgba(255, 99, 132, 0.2)",
            "rgba(54, 162, 235, 0.2)",
            "rgba(255, 206, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            //"rgba(255, 159, 64, 0.2)",
          ],
          borderColor: [
            "rgba(255, 99, 132, 1)",
            "rgba(54, 162, 235, 1)",
            "rgba(255, 206, 86, 1)",
            "rgba(75, 192, 192, 1)",
            "rgba(153, 102, 255, 1)",
            //"rgba(255, 159, 64, 1)",
          ],
          borderWidth: 1,
        },
      ],
    },
    options: {
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
            },
          },
        ],
      },
      legend: {
        display: false,
      },
    },
  });
});
