import { SwalNotification } from "./SwalNotification";

//-------------------------------------------------------------
try {
  window.Popper = require("popper.js").default;
  window.$ = window.jQuery = require("jquery");

  require("bootstrap");
  //Otro(s) posibles archivo(s) a requerir
  //----------------------------------------------
  //vale pasar windows.jQuery tanto así
  //como sin windows. lo mismo para $
  ////require('jquery.easing')(window.jQuery);
  ////require('jquery.easing')($);
} catch (e) {}
//-------------------------------------------------------------

document.addEventListener("DOMContentLoaded", domReady());

function domReady() {
  changingNavbarOnScrolling();
}

function changingNavbarOnScrolling() {
  window.addEventListener("scroll", function () {
    let getUrl = window.location;
    let baseUrl = getUrl.protocol + "//" + getUrl.host + "/";

    let nav = document.querySelector("nav");
    let imgLogo = document
      .getElementsByClassName("logo")[0]
      .getElementsByTagName("img")[0];

    nav.classList.toggle("sticky", window.scrollY > 0);

    if (window.scrollY > 0) {
      imgLogo.src = baseUrl + "images/logo-amie-web-2d2.png";
    } else {
      imgLogo.src = baseUrl + "images/logo-amie-web.png";
    }
  });
}

// Registro global de variable/función
// ----------------------------------------------------------

window.SwalNotification = new SwalNotification();
