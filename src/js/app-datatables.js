//-------------------------------------------------------------
try {
  window.$ = window.jQuery = require("jquery");

  //DataTables
  require("datatables.net-bs4");
  require("datatables.net-buttons-bs4");
  // --------------------------------------
  // Pasando el jQuery al componente si hiciera falta
  // require("datatables.net-bs4")(window.$);
  // require("datatables.net-buttons-bs4")(window.$);
} catch (e) {}
//-------------------------------------------------------------
