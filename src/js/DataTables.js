/**
 * Configuración básica para aplicar DataTable
 * a la tabla referenciada.
 */

$("#tableList").DataTable({
  processing: true,
  language: {
    url: "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
  },
});
