const mix = require("laravel-mix");

mix
  .js("src/js/app.js", "js")
  .js("src/js/app-datatables.js", "js")
  .js("src/js/Charts.js", "js")
  .js("src/js/DataTables.js", "js")
  .js("src/js/DataTables-Admin.js", "js/admin")
  .js("src/js/jquery.tabledit.min.js", "js/admin")
  // .postCss("src/css/app.css", "css", [
  .postCss("src/css/app-datatables.css", "css", [
    // listar todos los plugins para PostCSS necesarios...
    //require("postcss-import"),
    // require("postcss-custom-properties"),
    // o dejar este Array vacío si no se va a emplear plugin alguno
  ])
  //.sass("src/css/app-datatables.css", "css")
  .sass("src/scss/app.scss", "css")
  .setPublicPath("public");

if (!mix.inProduction()) {
  // Mapeo de fuentes de archivo
  mix.sourceMaps();
}

// Disable mix-manifest.json
Mix.manifest.refresh = (_) => void 0;
