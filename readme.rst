#####################################################
Proyecto Práctico con CodeIgniter 3.1.11 :: versión-2
#####################################################

El proyecto de este repositorio ha seguido el desarrollo del proyecto almacenado en este otro repositorio:

`amie.consulting-salarios <https://bitbucket.org/PJ-CM/amie.consulting-salarios/src/master/>`_

En este proyecto, entre otras cosas y a grandes rasgos:

- Se ha implementado un sistema de tablas de listado con el uso de `DataTables <https://datatables.net>`_ (listado, carga de datos por AJAX con Server Side Processing, edición y borrados de registros y actualización de la respectiva tabla).

- Se ha implementado alguna gráfica de datos mediante el uso de la librería de `Chart.js <https://www.chartjs.org>`_.


Nada más ... Saludos.
