/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/js/DataTables.js":
/*!******************************!*\
  !*** ./src/js/DataTables.js ***!
  \******************************/
/***/ (() => {

eval("/**\n * Configuración básica para aplicar DataTable\n * a la tabla referenciada.\n */\n$(\"#tableList\").DataTable({\n  processing: true,\n  language: {\n    url: \"//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json\"\n  }\n});//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9hbWllLmNvbnN1bHRpbmctc2FsYXJpb3MvLi9zcmMvanMvRGF0YVRhYmxlcy5qcz82ZDhkIl0sIm5hbWVzIjpbIiQiLCJEYXRhVGFibGUiLCJwcm9jZXNzaW5nIiwibGFuZ3VhZ2UiLCJ1cmwiXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUFBLENBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JDLFNBQWhCLENBQTBCO0FBQ3hCQyxFQUFBQSxVQUFVLEVBQUUsSUFEWTtBQUV4QkMsRUFBQUEsUUFBUSxFQUFFO0FBQ1JDLElBQUFBLEdBQUcsRUFBRTtBQURHO0FBRmMsQ0FBMUIiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENvbmZpZ3VyYWNpw7NuIGLDoXNpY2EgcGFyYSBhcGxpY2FyIERhdGFUYWJsZVxuICogYSBsYSB0YWJsYSByZWZlcmVuY2lhZGEuXG4gKi9cblxuJChcIiN0YWJsZUxpc3RcIikuRGF0YVRhYmxlKHtcbiAgcHJvY2Vzc2luZzogdHJ1ZSxcbiAgbGFuZ3VhZ2U6IHtcbiAgICB1cmw6IFwiLy9jZG4uZGF0YXRhYmxlcy5uZXQvcGx1Zy1pbnMvOWRjYmVjZDQyYWQvaTE4bi9TcGFuaXNoLmpzb25cIixcbiAgfSxcbn0pO1xuIl0sImZpbGUiOiIuL3NyYy9qcy9EYXRhVGFibGVzLmpzLmpzIiwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/js/DataTables.js\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./src/js/DataTables.js"]();
/******/ 	
/******/ })()
;