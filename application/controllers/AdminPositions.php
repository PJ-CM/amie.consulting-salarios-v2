<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AdminPositions extends AppBladeController
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('position_model');
    }

    public function getAllAjax()
    {
        $records = $this->position_model->getAll();
        $data    = [
            'records' => $records,
        ];

        echo json_encode($data);
    }
}
