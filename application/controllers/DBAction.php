<?php

defined('BASEPATH') or exit('No direct script access allowed');

class DBAction extends AppBladeController
{
    private $errors;
    private $msg;

    public function __construct()
    {
        parent:: __construct();
        $this->load->library('session');
        $this->errors = [];
        $this->msg    = '';
    }

    public function migrate()
    {
        $this->load->library('migration');

        if ($this->migration->current() === false) {
            show_error($this->migration->error_string());
        } else {
            $arr_response = [
                'result' => 'success',
                'msg'    => 'Proceso de Migraciones satisfactorio.',
            ];
            $this->session->set_flashdata('processReturn', $arr_response);

            redirect(base_url('admin'));
        }
    }

    public function executeFactory($model)
    {
        $this->errors = [];
        $modelName    = strtoupper($model) . 'S';
        $result       = ['success', 'error'];

        if (!$this->isModelAllow($model)) {
            $this->errors[] = 'ERROR - El MODELO pasado no existe.';
        } else {
            try {
                switch ($model) {
                case 'position':
                    //---------------------------
                    /*
                    $this->load->model('position_model');
                    $data = $this->position_model->getAll();
                    ////print_r($data);
                    echo '<br><br><br>';
                    $index = array_rand($data);
                    echo $data[$index]->id . ' ' . $data[$index]->description;
                    // print_r(array_rand($data));
                    echo '<br><br><br>';
                    // print_r(array_rand($data));
                    $index = array_rand($data);
                    echo $data[$index]->id . ' ' . $data[$index]->description;
                    //print_r($data[0]->id);
                    //print_r($data[0]);
                    exit;*/
                    //---------------------------
                    $this->load->library('factories/PositionFactory', '', 'positionFactory');
                    $this->positionFactory->executeFactory();
                    break;

                case 'employee':
                    $this->load->model('position_model');
                    $data = $this->position_model->getAll();
                    if (count($data) == 0) {
                        $this->errors[] = 'ERROR - Deben existir datos en POSITIONS primero.';
                        break;
                    }

                    $this->load->library('factories/EmployeeFactory', '', 'employeeFactory');
                    $this->employeeFactory->executeFactory();
                    break;

                default:
                    // code...
                    break;
            }
            } catch (Exception $e) {
                $this->errors[] = $e->getMessage();
            }
        }

        if (count($this->errors) == 0) {
            $result    = $result[0];
            $this->msg = "OK - Datos cargados en $modelName.";
        } else {
            $result    = $result[1];
            $this->msg = $this->errors[0];
        }
        $arr_response = [
            'result' => $result,
            'msg'    => $this->msg,
        ];
        $this->session->set_flashdata('processReturn', $arr_response);

        redirect(base_url('admin'));

        // return false;
        // o
        exit;
    }

    public function isModelAllow($model)
    {
        $_arr_modelsAllowed = ['position', 'employee'];

        return in_array($model, $_arr_modelsAllowed) ? true : false;
    }
}
