<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Salarios extends AppBladeController
{
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('Salarios_model');
    }

    public function index()
    {
        // $empleados = $this->Salarios_model->obtenerEmpleados();
        // $datos = array(
        // 	'empleados' => $empleados
        // );
        // $this->load->view('salarios_view', $datos);

        $records = $this->Salarios_model->getEmployeesPositions();
        $data    = [
            'records' => $records,
        ];

        // $this->load->view('salarios_view');
        $this->view('salaries.index', $data);
    }

    public function charts()
    {
        $this->view('salaries.charts');
    }

    public function chartMaxMinAvgSalaries()
    {
        $data = $this->Salarios_model->getMaxMinAvgSalaries();
        echo json_encode($data);
    }

    public function chartTotEmployeesByAgeRange()
    {
        $data = $this->Salarios_model->getTotEmployeesByAgeRange();
        echo json_encode($data);
    }

    public function chartAverageSalaryByAgeRange()
    {
        $data = $this->Salarios_model->getAverageSalaryByAgeRange();
        echo json_encode($data);
    }

    public function dataTableGetAll()
    {
        $records      = $this->Salarios_model->getEmployeesPositionsAjax();
        $data         = [
            'records' => $records,
        ];

        echo json_encode($data);
    }

    public function dataTableGetAllDtServerSideProcessing()
    {
        /**
         * Datos necesarios para la carga dinámica del DataTable implicado
         */
        $start  = $this->input->post('start');
        $length = $this->input->post('length');
        $search = $this->input->post('search')['value'];
        $order  = $this->input->post('order');

        $result      = $this->Salarios_model->getEmployeesPositionsAjaxDtServerSideProcessing($start, $length, $search, $order);

        $result_data       = $result['result_data'];
        $number_filter_row = $result['number_filter_row'];

        $data = [];
        foreach ($result_data->result_array() as $row) {
            $sub_array                     = [];
            // [ SIN EL Tabledit... ]
            // $sub_array['id']               = $row['id'];
            // $sub_array['name']             = $row['name'];
            // $sub_array['age']              = $row['age'];
            // $sub_array['registrationDate'] = $row['registrationDate'];
            // $sub_array['description']      = $row['description'];
            // $sub_array['salary']           = $row['salary'];
            //--------------------------------------------------------
            // [ CON EL Tabledit... ]
            $sub_array[]                   = $row['id'];
            $sub_array[]                   = $row['name'];
            $sub_array[]                   = $row['age'];
            // $sub_array[]                   = date('Y-m-d', strtotime($row['registrationDate']));
            $sub_array[]                   = date('d/m/Y', strtotime($row['registrationDate']));
            $sub_array[]                   = $row['description'];
            $sub_array[]                   = $row['salary'];
            $sub_array[]                   = $row['position_id'];
            $data[]                        = $sub_array;
        }

        $records_total = $result_data->num_rows();

        $json_data         = [
            'draw'             => intval($this->input->post('draw')),
            'recordsTotal'     => intval($records_total),
            'recordsFiltered'  => intval($number_filter_row),
            'data'             => $data,
        ];

        echo json_encode($json_data);
    }

    public function dataTableGetAllDtTedSSPActions()
    {
        // NO
        // **************
        // $data = json_decode(file_get_contents('php://input'), true);
        // //echo $data;
        // var_dump('DATOS: ', $data);
        // //return $data;
        // // echo json_decode([$this->input->post()]);
        // exit;
        //----------------------------------------------------------
        // NO
        // **************
        // $this->output->set_content_type('application/json');
        //----------------------------------------------------------
        // ¿?
        // **************
        //header('Content-type:application/json');

        //-> Con el jQuery-Tabledit-markcell-OK
        //-> Con el jQuery-Tabledit-bluesatKV-NOK
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // var_dump('MÉTODO-POST');
            $input = filter_input_array(INPUT_POST);
        // var_dump('POSTInput>>', $input);
        // var_dump('POST>>', $_POST);
        } elseif ($_SERVER['REQUEST_METHOD'] == 'GET') {
            // var_dump('MÉTODO-GET', $_GET);
        }

        //----------------------------------------------------------
        // --
        // **************¿?
        // $json             = $this->input->raw_input_stream;
        // $json             = json_decode($json);

        //var_dump('DATOS_Post: ', $json);
        //var_dump(json_decode(trim(file_get_contents('php://input')), true));
        ////exit;

        if ($this->input->post('action') == 'edit') {
            // var_dump('Estoy AKI :: action >>', $this->input->post('action'));
            // exit;
            // Formateando fecha para ingreso a BD...
            $registrationDate  = $this->input->post('registrationDate');
            $registrationDate  = str_replace('/', '-', $registrationDate);
            $registrationDate  = date('Y-m-d', strtotime($registrationDate));

            $data = [
                'registrationDate'      => $registrationDate,
                'position_id'           => $this->input->post('position_id'),
                'salary'                => $this->input->post('salary'),
                'id'                    => $this->input->post('id'),
            ];

            $this->Salarios_model->updateFieldsEmployeesPositionsAjaxDtTed($data);

            echo json_encode($this->input->post());
            // echo json_encode($this->input->post(null, true));
        }

        if ($this->input->post('action') == 'delete') {
            $data = [
                'id'     => $this->input->post('id'),
            ];

            $this->Salarios_model->deleteFieldsEmployeesPositionsAjaxDtTed($data);

            echo json_encode($this->input->post());
            // echo json_encode($this->input->post(null, true));
        }
    }
}
