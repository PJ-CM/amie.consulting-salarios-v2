<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends AppBladeController
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('session');
    }

    public function index()
    {
        $this->view('admin.index');
    }
}
