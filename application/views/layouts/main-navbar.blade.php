    {{-- <nav class="navbar navbar-expand-lg navbar-light bg-darky d-flex justify-content-around">
        <a class="navbar-brand" href="/" title="Amie Consulting S.L.">
            <img src="images/logo-amie-web-2d2.png" class="d-inline-block align-top" alt="Amie Consulting S.L.">
        </a>
        <div class="collapse navbar-collapse navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin">Admin</a>
                </li>
            </ul>
        </div>
    </nav> --}}


    <nav>
        <a href="/" class="logo" title="Amie Consulting S.L.">
            <img src="{{ base_url() }}images/logo-amie-web.png" class="d-inline-block align-top" alt="Amie Consulting S.L.">
        </a>
        <ul>
            <li><a href="{{ base_url() }}"@if (uri_string() == '') class="active" title="Estás en Home"@else title="Ir a Home" @endif>Home</a></li>
            <li><a href="{{ base_url() }}charts"@if (uri_string() == 'charts') class="active" title="Estás en Gráficas"@else title="Ir a Gráficas" @endif>Gráficas</a></li>
            <li><a href="{{ base_url() }}admin"@if (uri_string() == 'admin') class="active" title="Estás en ADMIN"@else title="Ir a ADMIN" @endif>[Admin]</a></li>
        </ul>
    </nav>
