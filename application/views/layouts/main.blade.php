<?php
defined('BASEPATH') or exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> @yield('title') </title>

    @yield('head_extra_scritps')

    <style type="text/css">

        /* Tu propio estilo aquí */

    </style>

    <link rel="stylesheet" href="{{ base_url() }}css/app.css">
</head>
<body>

    @include('layouts.main-navbar')
    <div class="container-fluid">
        @yield('content')
    </div>

    <footer>
        <div class="copyright">
            &copy; {{ date('Y') }} &bull; Amie Consulting
        </div>
    </footer>

    <script type="text/javascript">

        // Tu javascript aquí

    </script>
    <script src="{{ base_url() }}js/app.js"></script>
    {{-- Opción 1d2 --}}
    {{-- @yield('footer_scripts_content') --}}
    {{-- Opción 2d21 --}}
    @stack('scripts')
</body>
</html>
