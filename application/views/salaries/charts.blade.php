@extends('layouts.main')

@section('title', 'Salarios - Gráficas')

@section('content')
    <div class="card">
        <div class="text-center card-header">
            <h3 class="card-title">Gráficas</h3>
        </div>
        <div class="card-body" style="min-height: 600px !important;">
            <!-- Cuerpo del CARD -->
            <div class="avg-chart-container">
                <h5>Promedio de Sueldos</h5>
                <canvas id="salariesMaxMinAvgChart" width="500" height="100"></canvas>
            </div>
            <div class="doughnuts-chart-container">
                <div>
                    <h5>Total de Empleados por Edades</h5>
                    <canvas id="totEmployeesByAgeRangeChart" width="500" height="200"></canvas>
                </div>
                <div>
                    <h5>Sueldo Medio por Rangos de Edad</h5>
                    <canvas id="averageSalaryByAgeRangeChart" width="500" height="200"></canvas>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="js/Charts.js"></script>
@endpush
