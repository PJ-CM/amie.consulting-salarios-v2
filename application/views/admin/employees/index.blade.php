@extends('layouts.main')

@section('title', 'Empleados - CRUD')

@section('head_extra_scritps')
    <link rel="stylesheet" href="{{ base_url() }}css/app-datatables.css">


    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
@endsection

@section('content')
    <div class="card">
        <div class="text-center card-header">
            <h3 class="card-title">Salarios de los Empleados - Actualización de Datos</h3>
        </div>
        <div class="card-body" style="min-height: 600px !important;">
            <!-- Cuerpo del CARD -->
            <table class="table table-hover" id="tableListAndEdit">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Edad</th>
                        <th scope="col">Fecha Alta</th>
                        <th scope="col">Puesto</th>
                        <th scope="col">Salario</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ base_url() }}js/app-datatables.js"></script>
    <script type="text/javascript" src="{{ base_url() }}js/admin/DataTables-Admin.js"></script>

    <script src="{{ base_url() }}js/admin/jquery.tabledit.min.js"></script>
@endpush
