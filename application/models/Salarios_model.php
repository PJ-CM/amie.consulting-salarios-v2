<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Salarios_model extends CI_Model
{
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
        $this->table_employees = 'employees';
        $this->table_positions = 'positions';
    }

    public function getEmployeesPositions()
    {
        $this->db->select('table_1.*, table_2.description');
        $this->db->from($this->table_employees . ' AS table_1');
        $this->db->join($this->table_positions . ' AS table_2', 'table_1.position_id = table_2.id');
        $this->db->order_by('table_1.id');
        $query = $this->db->get();
        return $query->result();
    }

    public function getMaxMinAvgSalaries()
    {
        // ¿Por qué no sale si se le aplica el FORMAT() para limitar los decimales?
        // $sql = "SELECT FORMAT(SUM(salary), 2) AS 'suma', FORMAT(MAX(salary), 2) AS 'max', FORMAT(MIN(salary), 2) AS 'min', FORMAT(AVG(salary), 2) AS 'avg' FROM " . $this->table_employees;
        //---------------------------
        $sql = "SELECT SUM(salary) AS 'suma', MAX(salary) AS 'max', MIN(salary) AS 'min', AVG(salary) AS 'avg' FROM " . $this->table_employees;
        return $this->db->query($sql)->result();
    }

    public function getTotEmployeesByAgeRange()
    {
        $sql = "SELECT empl.TOT FROM ( SELECT COUNT(*) AS TOT FROM $this->table_employees WHERE age BETWEEN 18 and 30 UNION SELECT COUNT(*) AS TOT FROM $this->table_employees WHERE age BETWEEN 31 and 50 UNION SELECT COUNT(*) AS TOT FROM $this->table_employees WHERE age > 50 ) AS empl";
        return $this->db->query($sql)->result();
    }

    public function getAverageSalaryByAgeRange()
    {
        $sql = "SELECT empl.age_rango, empl.avg_salary FROM ( SELECT 'de 18 a 25' AS age_rango, AVG(salary) AS avg_salary FROM $this->table_employees WHERE age BETWEEN 18 and 25 UNION ALL SELECT 'de 26 a 35' AS age_rango, AVG(salary) AS avg_salary FROM $this->table_employees WHERE age BETWEEN 26 and 35 UNION ALL SELECT 'de 36 a 45' AS age_rango, AVG(salary) AS avg_salary FROM $this->table_employees WHERE age BETWEEN 36 and 45 UNION ALL SELECT 'de 46 a 55' AS age_rango, AVG(salary) AS avg_salary FROM $this->table_employees WHERE age BETWEEN 46 and 55 UNION ALL SELECT '+55' AS age_rango, AVG(salary) AS avg_salary FROM $this->table_employees WHERE age > 55 ) AS empl";
        return $this->db->query($sql)->result();
    }

    public function getEmployeesPositionsAjax()
    {
        $this->db->select('table_1.*, table_2.description');
        $this->db->from($this->table_employees . ' AS table_1');
        $this->db->join($this->table_positions . ' AS table_2', 'table_1.position_id = table_2.id');
        $this->db->order_by('table_1.id');
        $query = $this->db->get();
        return $query->result();
    }

    // public function getEmployeesPositionsAjaxDTServerSideProcessing($start, $length, $search, $order)
    // {
    //     $this->db->select('table_1.*, table_2.description');
    //     $this->db->from($this->table_employees . ' AS table_1');
    //     $this->db->join($this->table_positions . ' AS table_2', 'table_1.position_id = table_2.id');
    //     $this->db->order_by('table_1.id');
    //     $query = $this->db->get();
    //     return $query->result();
    // }

    public function getEmployeesPositionsAjaxDtServerSideProcessing($start, $length, $search, $order)
    {
        $columnsOrder = ['id', 'name', 'age', 'registrationDate', 'description', 'salary'];

        $q_a = "SELECT table_1.*, table_2.id AS position_id, table_2.description FROM $this->table_employees AS table_1 INNER JOIN $this->table_positions AS table_2 ON table_1.position_id = table_2.id";

        if ($search) {
            $q_a .= " WHERE table_1.id LIKE '%$search%'";
            $q_a .= " OR name LIKE '%$search%'";
            $q_a .= " OR age LIKE '%$search%'";
            $q_a .= " OR registrationDate LIKE '%$search%'";
            $q_a .= " OR description LIKE '%$search%'";
            $q_a .= " OR salary LIKE '%$search%'";
        }

        if ($order) {
            $q_a .= ' ORDER BY ' . $columnsOrder[$order['0']['column']] . ' ' . $order['0']['dir'];
        } else {
            $q_a .= ' ORDER BY id ASC';
        }

        $q_b = '';
        if ($length != -1) {
            $q_b .= " LIMIT $start, $length";
        }

        $number_filter_row = $this->db->query($q_a)->num_rows();

        $result = $this->db->query($q_a . $q_b);

        $return_data = [
            'number_filter_row' => $number_filter_row,
            'result_data'       => $result,
        ];

        return $return_data;
    }

    public function updateFieldsEmployeesPositionsAjaxDtTed($data)
    {
        $sql = "UPDATE $this->table_employees SET registrationDate = ?, position_id = ?, salary = ? WHERE id = ?";
        $this->db->query($sql, $data);
    }

    public function deleteFieldsEmployeesPositionsAjaxDtTed($data)
    {
        $this->db->delete($this->table_employees, $data);
    }
}
