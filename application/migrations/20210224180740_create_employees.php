<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_employees extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'name' => [
                'type'       => 'VARCHAR',
                'constraint' => '150',
            ],
            'age' => [
                'type'       => 'INT',
                'constraint' => 3,
            ],
            'registrationDate' => [
                'type'       => 'DATETIME',
                'constraint' => 5,
            ],
            'position_id' => [
                'type'       => 'INT',
                'constraint' => 5,
                'unsigned'   => true,
            ],
            'salary' => [
                'type'       => 'DECIMAL',
                'constraint' => '10,2',
                'default'    => 0.00,
            ],
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('employees');
        $this->db->query('ALTER TABLE employees ADD CONSTRAINT position_id_fk FOREIGN KEY(`position_id`) REFERENCES positions(`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
    }

    public function down()
    {
        $this->dbforge->drop_table('employees');
    }
}
