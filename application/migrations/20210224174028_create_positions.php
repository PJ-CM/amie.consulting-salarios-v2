<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Create_positions extends CI_Migration
{
    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type'           => 'INT',
                'constraint'     => 5,
                'unsigned'       => true,
                'auto_increment' => true,
            ],
            'description' => [
                'type'       => 'VARCHAR',
                'constraint' => '150',
            ],
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('positions');
    }

    public function down()
    {
        $this->dbforge->drop_table('positions');
    }
}
